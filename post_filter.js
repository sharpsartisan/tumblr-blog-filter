//
//  post_filter.js
//  Tumblr Blog Filter
//  bitbucket: sharpsartisan/tumblr-blog-filter
//
//  Created by Oliver Bredemeyer on 12/09/2015.
//  Copyright (c) 2015 Oliver Bredemeyer and Alexis Shepherd. All rights reserved.
//



// SINGLETON util
// DESCRIPTION general utilities for data processing
var util = {};

// util.extract (Object) -> [[String/Number,Any]]
// DESCRIPTION transforms an object into an array of key-value pairs
util.extract = function (obj) {
    return Object.keys(obj).map(function(key){
        // (String/Number key) -> [String/Number key, Any value]
        return [key,obj[key]];
    });
};



// SINGLETON post_filter
// DESCRIPTION main API interface
var post_filter = {

    // {String postType : Boolean show} current: current filter status
    current: {
        photo: true, video: true, audio: true, quote: true,
        text: true, answer: true, chat: true, link: true
    }

};

// post_filter.set ({String type : Boolean show}) -> ()
// DESCRIPTION changes post hiding
post_filter.set = function (delta) {

    var $this = this;

    util.extract(delta).map(function(cmd){
        // DESCRIPTION hides or shows all posts of type by status
        // ([String type, Boolean newStatus]) -> ()
        var flags = document.querySelectorAll('input.' + cmd[0] + '-post-flag'); // :NodeList
        var posts = Array.prototype.map.call(flags,function(node){
                // DESCRIPTION maps a flag to its parent post
                // (Node) -> Node
                return node.parentNode;
            }); // :[Node]

        posts.forEach(function(node){
            // DESCRIPTION show or hide post
            // (Node) -> ()
            node.style.display = cmd[1] ? 'block' : 'none';
        });

        $this.current[cmd[0]] = cmd[1];

    });

};

// post_filter.showOnly (String type) -> ()
// DESCRIPTION hides all but one type of post
post_filter.showOnly = function (type) {

    var delta = {
        photo: false, video: false, audio: false, quote: false,
        text: false, answer: false, chat: false, link: false
    }; // :{String:Boolean}

    delta[type] = true;

    this.set(delta);

};

// post_filter.showAll () -> ()
// DESCRIPTION shows all post types
post_filter.showAll = function () {
    this.set({
        photo: true, video: true, audio: true, quote: true,
        text: true, answer: true, chat: true, link: true
    });
};

post_filter.intervalID = window.setInterval(function(){
    post_filter.set(post_filter.current);
},500);


